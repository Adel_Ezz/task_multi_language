<?php
/**
 * Created by PhpStorm.
 * User: ENG Ezz
 * Date: 22/06/2018
 * Time: 02:32 م
 */


use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(App\Company::class, function ($faker) {

    return [
        'name' =>$faker->sentence,
        'logo' => $faker->sentence,
        'email' => $faker->sentence,
    ];
});


