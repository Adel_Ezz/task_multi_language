@if(session()->has('success'))
    <div  class="alert notification alert-success text-center">
        <h3>{{ session()->get('success') }}</h3>
    </div>
@endif


@if(session()->has('error'))
    <div class="alert notification alert-danger text-center">
        <h3>{{ session()->get('error') }}</h3>
    </div>
@endif