<!-- Jquery Core Js -->
<script src="{{ asset('cpanel/plugins/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap Core Js -->
<script src="{{ asset('cpanel/plugins/bootstrap/js/bootstrap.js')}}"></script>
<!-- Slimscroll Plugin Js -->
<script src="{{ asset('cpanel/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<!-- Waves Effect Plugin Js -->
<script src="{{ asset('cpanel/plugins/node-waves/waves.js')}}"></script>
<!-- Jquery CountTo Plugin Js -->
<script src="{{ asset('cpanel/plugins/jquery-countto/jquery.countTo.js')}}"></script>


<!-- Sparkline Chart Plugin Js -->
<script src="{{ asset('cpanel/plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>
<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('cpanel/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{ asset('cpanel/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{ asset('cpanel/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('cpanel/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
<script src="{{ asset('cpanel/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
<script src="{{ asset('cpanel/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
<script src="{{ asset('cpanel/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
<script src="{{ asset('cpanel/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
<script src="{{ asset('cpanel/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
<!-- Custom Js -->
<script src="{{ asset('cpanel/js/admin.js')}}"></script>
@yield('tables')
<script src="{{ asset('cpanel/js/pages/index.js')}}"></script>


<!-- Demo Js -->
<script src="{{ asset('cpanel/js/demo.js')}}"></script>
</body>

</html>