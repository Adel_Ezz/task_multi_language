@extends('home')


@section('content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        {{ trans('main.companies') }}
                        <a href="{{ url('/home/employees/create') }}"><button type="button" class="btn btn-info waves-effect pull-right" style="margin-bottom: 20px;">{{ trans('main.add') }}</button></a>
                    </h2>

                </div>
                <div class="body">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th>{{ trans('main.name') }}</th>
                            <th>{{ trans('main.email') }}</th>
                            <th>{{ trans('main.phone') }}</th>
                            <th>{{ trans('main.action') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>{{ trans('main.name') }}</th>
                            <th>{{ trans('main.email') }}</th>
                            <th>{{ trans('main.phone') }}</th>
                            <th>{{ trans('main.action') }}</th>
                        </tr>

                        </tfoot>
                        <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <td>{{ upertext($employee->first_name) }}</td>
                                <td>{{ $employee->email }}</td>
                                <td>{{ $employee->phone }}</td>
                                <td>
                                    <a href="{{ url('home/employees/'.$employee->id.'/edit ') }}"
                                       type="button"
                                       class="btn bg-deep-purple btn-circle waves-effect waves-circle waves-float"
                                       title="تعديل">
                                        <i class="material-icons">settings</i>
                                    </a>
                                    <form action="{{ url('home/employees/'.$employee->id) }}"
                                          style="display:inline-block" method="post" class="deleteme">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit"
                                                class="btn bg-red btn-circle waves-effect waves-circle waves-float "
                                                title="حذف">
                                            <i class="material-icons">delete_forever</i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                   <div class=" text-center">
                       {{ $employees->appends(['sort' => 'votes'])->links() }}
                   </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
@endsection
@section('tables')
    <script src="{{ asset('cpanel/js/pages/tables/jquery-datatable.js')}}"></script>
@endsection