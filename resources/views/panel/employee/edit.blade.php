@extends('home')


@section('content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        {{ trans('main.companies') }}
                        <a href="{{ url('/home/companies/create') }}">
                            <button type="button" class="btn btn-info waves-effect pull-right"
                                    style="margin-bottom: 20px;">{{ trans('main.add') }}</button>
                        </a>
                    </h2>

                </div>
                <div class="body">


                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                        {{ trans('main.employees') }}
                                    </h2>
                                </div>
                                <div class="body">
                                    <form method="post" action="{{ url('/home/employees/'.$employee->id) }}"
                                          enctype="multipart/form-data">
                                        <input name="_method" type="hidden" value="PUT">
                                        @csrf
                                        {!! input('first_name','text',$employee->first_name) !!}
                                        {!! input('last_name','text',$employee->last_name) !!}
                                        <input type="email" name="email" value="{{ $employee->email }}"
                                               class="form-control"
                                               placeholder="{{ trans('main.email') }}" required><br>
                                        <input type="text" name="phone" value="{{ $employee->phone }}"
                                               class="form-control" placeholder="{{ trans('main.phone') }}"
                                               required><br>
                                        <label>Company</label>

                                        <select name="company" class="form-control">
                                            @foreach($companies as $company)
                                                <option value="{{ $company->id }}"
                                                        @if($company->id ==$employee->company->id)
                                                        selected
                                                        @endif
                                                >{{ upertext($company->name) }}</option>
                                            @endforeach
                                        </select>
                                        <button type="submit"
                                                class="btn btn-primary m-t-15 waves-effect">{{ trans('main.add') }}</button>
                                    </form>
                                    @if ($errors->any())
                                        @foreach ($errors->all() as $error)
                                            <div class="alert alert-danger">
                                                {{ $error }}
                                            </div>
                                        @endforeach
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
@endsection
