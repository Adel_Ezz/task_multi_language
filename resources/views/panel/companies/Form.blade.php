<label for="name">{{ trans('main.name') }}</label>
<div class="form-group">
    <div class="form-line">
        <input type="text" name="name" id="name" class="form-control" placeholder="{{ trans('main.putName') }}" >
    </div>
</div>
