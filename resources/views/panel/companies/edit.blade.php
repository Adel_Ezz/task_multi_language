@extends('home')


@section('content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        {{ trans('main.companies') }}
                        <a href="{{ url('/home/companies/create') }}">
                            <button type="button" class="btn btn-info waves-effect pull-right"
                                    style="margin-bottom: 20px;">{{ trans('main.add') }}</button>
                        </a>
                    </h2>

                </div>
                <div class="body">


                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                        {{ trans('main.companies') }}
                                    </h2>
                                </div>
                                <div class="body">
                                    <form method="post" action="{{ url('/home/companies/'.$company->id) }}"
                                          enctype="multipart/form-data">
                                        <input name="_method" type="hidden" value="PUT">
                                        @csrf
                                        {!! input('name','text',$company->name) !!}
                                        <input type="email" name="email" value="{{ $company->email }}" class="form-control"
                                               placeholder="{{ trans('main.email') }}" required><br>
                                        <input type="text" name="website" value="{{ $company->website }}"
                                               class="form-control" placeholder="{{ trans('main.website') }}"
                                               required><br>
                                        <label>logo</label>
                                        <img src="{{ asset('/app/storage/'.$company->logo) }}" width="150"
                                             height="100">

                                        <input type="file" name="logo" >

                                        <br>
                                        <button type="submit"
                                                class="btn btn-primary m-t-15 waves-effect">{{ trans('main.add') }}</button>
                                    </form>
                                    @if ($errors->any())
                                        @foreach ($errors->all() as $error)
                                            <div class="alert alert-danger">
                                                {{ $error }}
                                            </div>
                                        @endforeach
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
@endsection
