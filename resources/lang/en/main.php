<?php

return [

    'login' => 'login',
    'logOut' => 'LogOut',
    'laravel' => 'Laravel',
    'password' => 'Password',
    'email' => 'Email',
    'remember' => 'Remeber',
    'home' => 'Home',
    'dashboard' => 'Dashboard',
    'admin' => 'Admin',
    'name' => 'Name',
    'website' => 'Website',
    'action' => 'Actions',
    'companies' => 'Companies',
    'add' => 'Add',
    'putName' => 'Please put here your name',
    'done' => 'Process Done!!!',
    'phone' => 'Phone'

];