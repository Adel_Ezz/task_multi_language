<?php
return [

    'login' => 'تسجيل الدخول',
    'logOut' => 'تسجيل خروج',
    'laravel' => 'لارافيل',
    'password' => 'الرقم السرى',
    'email' => 'البريد',
    'remember'=>'تذكرنى',
    'home'=>'الرأيسة',
    'dashboard'=>'لوحة التحكم',
    'admin'=>'لوحة الأدمن',
    //////table of datatabel
    'name'=>'الإسم',
    'website'=>'الموقع الأكترونى',
    'action'=>'أفعال',
    'companies'=>'الشركات',
    'add'=>'إضافة',
    'putName'=>'من فضلك ضع الأسم هنا!!!',
    'done'=>'تمت العملية بنجاح',
    'phone'=>'الهاتف'


];