<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    //
    protected  $fillable=['id','first_name','last_name','email','phone','company_id'];

    public function company()
    {
        return $this->belongsTo(Company::class,'company_id');
    }
}
