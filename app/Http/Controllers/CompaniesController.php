<?php

namespace App\Http\Controllers;

use App\Company;
use App\Employee;

use App\Notifications\AddedNewCompany;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;

class CompaniesController extends Controller
{

    public static function input($name, $type, $value = null)
    {
        $input = '';
        foreach (app('lang') as $lang) {
            $input .= view('inputs.' . $type, ['name' => $name, 'lang' => $lang, 'value' => $value])->render();
        }
        return $input;
    }


    public static function getcon($name)
    {
        $values = [];
        foreach (app('lang') as $lang) {
            $values[$lang] = request($lang . '_' . $name);
        }
        return json_encode($values);
    }


    public static function upertext($val)
    {
        $value = json_decode($val);
        $current = app('langs');
        return $value->$current;
    }
    public static function upertextshow($name,$val,$language='')
    {
        if ($val != null && $val !='')
        {
            $value = json_decode($val);

            if ($language != '')
            {
                $current = $language ;

            }
            else
            {
                $current = app('langs');
            }
            return $value->$current;
        }
        else
            {
            return old($name);
        }

    }



    public function index()
    {
        //
        $companies = Company::paginate(10);

        return view('panel.companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('panel.companies.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'website' => 'required',
            'logo' => 'required|mimes:jpg,jpeg,png|dimensions:min_width=100,min_height=100'
        ]);
        $company = new Company();
        $photo = $request->logo;
        if ($request->has('logo')) {
//            $old_photo = base_path() . '/public/app/storage/' . $photo;
//            if (file_exists($old_photo)) {
//                unlink($old_photo);
//            }
            $filename = time() . '_' . $photo->getClientOriginalName();
            $photo->move(base_path() . '/public/app/storage/', $filename);
            $company->logo = $filename;
        }

        $company->email = $request->email;
        $company->website = $request->website;
        $company->name = $this::getcon('name');
        $company->save();
        $new=$company->find($company->id);
        ////////////////////For Sending Notifications to user//////////
        $user =Auth::user();
        \Notification::send($user, new \App\Notifications\AddedNewCompany($user,$new) );

        return back()->with('success',trans('main.done'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company $companies
     * @return \Illuminate\Http\Response
     */
    public function show(Company $companies)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company $companies
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $companies,$id)
    {
        //

        $company=Company::find($id);
        return view('panel.companies.edit',compact('company'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Company $companies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id, Company $companies)
    {
        $this->validate($request, [
            'email' => 'required',
            'website' => 'required',
            'logo' => 'mimes:jpg,jpeg,png|dimensions:min_width=100,min_height=100'
        ]);
        $company = $companies->find($id);

        if ($request->has('logo')) {
            $photo = $request->logo;
            $old_photo = base_path() . '/public/app/storage/'.$company->logo;
            if (file_exists($old_photo)) {
                unlink($old_photo);
            }
            $filename = time() . '_' . $photo->getClientOriginalName();
            $photo->move(base_path() . '/public/app/storage/', $filename);
            $company->logo = $filename;
        }

        $company->email = $request->email;
        $company->website = $request->website;
        $company->name = $this::getcon('name');
        $company->save();
        return back()->with('success',trans('main.done'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company $companies
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $companies,$id)
    {
        $company = $companies->find($id);
        $employees=Employee::where('company_id',$id)->get();
        foreach ($employees as $employee)
        {
            $employee->delete();
        }
        $company->delete();
        return back()->with('success',trans('main.done'));
        //

    }
}
