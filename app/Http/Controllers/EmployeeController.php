<?php

namespace App\Http\Controllers;

use App\Company;
use App\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $employees = Employee::paginate(10);

        return view('panel.employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
        $companies = Company::get();
        return view('panel.employee.add', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'company' => 'required',
            'phone' => 'required',
            'ar_first_name' => 'required',
            'ar_last_name' => 'required',
            'en_first_name' => 'required',
            'en_last_name' => 'required'
        ]);
        $employee = new Employee();
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->company_id = $request->company;
        $employee->first_name = getcon('first_name');
        $employee->last_name = getcon('last_name');
        $employee->save();
        return back()->with('success', trans('main.done'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\employee $employee
     * @return \Illuminate\Http\Response
     */
    public function show(employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\employee $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $employee = Employee::find($id);
        $companies = Company::get();
        return view('panel.employee.edit', compact('employee','companies'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request, [
            'email' => 'required',
            'company' => 'required',
            'phone' => 'required',
            'ar_first_name' => 'required',
            'ar_last_name' => 'required',
            'en_first_name' => 'required',
            'en_last_name' => 'required'
        ]);
        $employee = Employee::find($id);

        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->company_id = $request->company;
        $employee->first_name = getcon('first_name');
        $employee->last_name = getcon('last_name');
        $employee->save();
        return back()->with('success', trans('main.done'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\employee $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Employee::find($id)->delete();;
        return back()->with('success', trans('main.done'));
        //
    }
}
