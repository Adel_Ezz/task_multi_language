<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//==========For languages that site have====///
app()->singleton('lang', function () {
    return ['ar', 'en'];
});
///===================single Tone End=======///

///========For set local language depending on session or language of auth user===/////////////
app()->singleton('langs', function () {
    if (auth()->user()) {
        if (empty(auth()->user()->lang)) {
            return 'en';
        } else {
            return auth()->user()->lang;
        }
    } else {
        if (session()->has('lang')) {
            return session('lang');
        } else {
            return 'en';
        }
    }
});

Route::group(['middleware' => 'lang'], function () {
    Auth::routes();

    Route::get('/', function () {
        return view('welcome');
    });


    Route::group(['middleware'=>'auth'],function (){
        Route::get('/home', 'HomeController@index')->name('home');
        Route::group(['prefix'=>'/home'],function(){
            Route::resource('/companies','CompaniesController');
            Route::resource('/employees','EmployeeController');
        });

    });



});
////for chossing language/////
Route::get('lang/{lang}', function ($lang) {

    if (in_array($lang, ['ar', 'en'])) {
        if (auth()->user()) {
            $user = auth()->user();
            $user->lang = $lang;
            $user->save();
        } else {
            if (session()->has('lang')) {
                session()->forget('lang');
            }
            session()->put('lang', $lang);
        }

    } else {
        if (auth()->user()) {
            $user = auth()->user();
            $user->lang = 'en';
            $user->save();
        } else {
            if (session()->has('lang')) {
                session()->forget('lang');
            }
            session()->put('lang', $lang);
        }
    }
    return back();
});






